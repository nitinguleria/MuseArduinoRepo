package commusenitinguleriaMuseListeners;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.widget.TextView;

import com.interaxon.libmuse.ConnectionState;
import com.interaxon.libmuse.MuseConnectionListener;
import com.interaxon.libmuse.MuseConnectionPacket;
import com.muse.nitinguleria.dataloggingmuse.R;

import java.lang.ref.WeakReference;


public class ConnectionListener extends MuseConnectionListener {

    //set status
    TextView statusTextView;




    final WeakReference<Activity> activityRef;

    public ConnectionListener(final WeakReference<Activity> activityRef) {
        this.activityRef = activityRef;
    }

    @Override
    public void receiveMuseConnectionPacket(MuseConnectionPacket p) {
        final ConnectionState current = p.getCurrentConnectionState();
        final String status ="Status : "+ p.getPreviousConnectionState().toString() +
                " -> " + current;
        final String full = "Muse " + p.getSource().getMacAddress() +
                " " + status;
        Log.i("Muse Headband", full);
        final Activity activity = activityRef.get();
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                     statusTextView =
                           (TextView) activity.findViewById(R.id.statusTextView);
                    statusTextView.setText(status);
                    statusTextView.setTextColor(Color.WHITE);
                }
            });
        }
    }



}
