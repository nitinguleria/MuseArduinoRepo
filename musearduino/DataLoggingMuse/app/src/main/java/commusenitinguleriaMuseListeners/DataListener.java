package commusenitinguleriaMuseListeners;

import android.app.Activity;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.interaxon.libmuse.Eeg;
import com.interaxon.libmuse.MuseArtifactPacket;
import com.interaxon.libmuse.MuseDataListener;
import com.interaxon.libmuse.MuseDataPacket;
import com.muse.nitin.guleria.arduinoConnection.BlueComms;
import com.muse.nitin.guleria.arduinoConnection.WifiSocketTask;
import com.muse.nitinguleria.dataloggingmuse.Matrix;
import com.muse.nitinguleria.dataloggingmuse.R;
import com.muse.nitinguleria.dataloggingmuse.SavetoFileThread;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;

/*  comm seperated data is generated with username_month_time with following data
   alpha_absolute [1..4]0-3
            *alpha_relative[1..4]4-7
            *beta_absolute[1..4]8-11
            * beta_relative[1..4]12-15
            *    16
            *delta_absolute[1..4]17-20
            *delta_relative[1..4]21-24
            *concentration 1 25
            *mellow1  26
            *8gamma_absolute[1..4]27-30
            *gamma_relative[1..4]31-34
            *theta_absolute[1..4]35-38
            *theta_relative[1..4]39-42
            * no Idea 43
             * Idea 44
              * Not Logged 45
            *
            * 16,25,26 set to 0
record eeg data*/
/*
*  listens to sensor data from muse
*
*  writes to text file in sdcard/ directory from muse
*
*  connect to arduino via wifi
*
*  does neural network calculation
*/

 public class DataListener extends MuseDataListener {




    //Data Listener Elements
    double[][] x1 = new double[43][1];
    final WeakReference<Activity> activityRef;
     SavetoFileThread savetoFileThread;

     // UI Elements
     TextView elem1,elem2,elem3,elem4;

    // Data Recording Elements
     Boolean connected=false;
     Boolean dataRecordingActivity =false;
     Boolean startSession=false;
     Boolean hasIdea =false;
     Boolean hasnoIdea =false;

     //log Elements
     Boolean firstTimeLog=true;
     String logfilename="Logdata";
     String logfileText="";


     //wifi  arduino input Elements
     private final String TAG = getClass().getSimpleName();
     TextView wifistatusTextView;
     String blinking ="0";
     String ideation ="0";
     Boolean wificonnected=false;
     Boolean wifiLightconnected= false;
     Boolean wifiActivity=false;
     ImageView wifibulbImageView;




     int counter=0;
     //ImageView righteyeImageView;


     //local WifiSocketThread
     WifiSocketTask wifiTask =null;
     private String arduinoIp=  "192.168.1.155";     //"192.168.1.179"; //"192.168.2.38";//
     private int port=23;


     //bluetooth
     Boolean bluetoothActivity=false;
     public BlueComms myBlueComms;
     ImageView bluetoothbulbImageView;
     Boolean bluetoothconnected=false;
     Boolean bluetoothsendData=true;
     TextView arduinoBluetoothStatusView;
     Boolean bluetoothopened=false;




     // Neural Network Elements
     double[][] xin = new double[40][1];
     double[][] x1_step1_xoffset=new double[40][1];//m =43 n=0
     double[][] x1_step1_gain=new double[40][1];//m =43 n=0
     double[][] x1_step1_ymin=new double[40][1];//m =43 n=0

     double[][] y1=new double[2][1];
     // ===== NEURAL NETWORK CONSTANTS =====
     //Input 1 1x40
     double x1_step_offset[]={-0.786519505641133,-0.805424574398295,-1.02713544627859,-0.450203192783945,0,0,0,0,-0.249702406824571,-0.977591240875352,-1.02602990653868,-0.368221100235875,0,0,0,0,-1.10719342959272,-1.08925201458063,-0.947909004654244,-1.18024008593782,0,0,0,0,-0.361061782284072,-1.55591170146024,-1.58762433740823,-0.432745738537018,0,0,0,0,-0.916334719131546,-1.15148440193923,-1.21825861980461,-1.07400544439774,0,0,0,0};
     double x1_step_gain[] = {1.48565119040992,1.84848387186225,1.4881784214874,2.17548787773979,4.93850337687473,7.56366847949863,4.90621584094597,4.69005292319839,2.19767044605303,1.20279147105409,1.52051223592326,2.38392179261792,3.56240964343476,3.75384953777306,3.86020619739857,4.32891260442741,1.32545155151249,1.26897011075371,1.04643959030776,1.05863906901591,5.98703846780274,2.56155642317234,2.3607935821487,3.56137380688272,2.41277618436995,0.830518686403517,1.04192968487305,2.27590393953515,5.4237494271397,2.96462230405958,4.75467705523967,5.15588750928698,1.61026111509674,1.37306386935004,1.35145685986329,1.26001846576817,7.21280332465297,7.26043259146556,7.82861663337781,4.60232815399444
     };

     //Layer1
     //5x1 only first row from 20x1
     double[][] b1={{1.4766569117281112},{-1.1958936807165292},{-1.1794662083458822},{-1.2362373038430474},{-1.0038159899720529}};
     //5x43 now  5x40 only first 5 from 20x43
     double[][] IW1_1={{-0.42203468866215815,-0.092871931805633731,-0.22945032531842813,0.26806660550804079,0.26520363657674556,0.42239807530218032,0.11187234950230687,-0.10237383651130025,-0.19030247299254305,-0.12302790501399596,0.35000415544001517,-0.21965256269407923,0.33836323477411528,0.20374685864567874,0.17000889179113682,-0.089020686048231865,0.36507897681353513,0.039898292484527122,-0.19389583422501952,-0.37819907910486306,-0.057193071910196638,-0.22190157922073137,0.20145164210338351,0.16619372742079028,0.027674424060367485,-0.060795472512521959,-0.33081901343554176,-0.061315412273264112,0.33558583578618795,0.055808300320263508,0.08306195609190728,0.12693148131015236,-0.026608892185220615,-0.3809560277761867,0.30646961347367407,-0.13783394720063086,-0.37829619495438621,-0.3311003922139158,0.069430953888980795,-0.31177324477434398},
             {0.2858607682909251,-0.67331106828345688,0.1515897265472517,0.50176594071090963,-0.34512944477383606,-0.076074296602243782,0.27919921015400506,-0.30537398503067575,0.46277875123174783,-0.53456666653321827,-0.27080184470920327,0.55755448792916784,0.53613939761851648,-0.62751257013438733,-0.65730864531727229,0.086109568412154613,-0.64939431070486964,0.32274464949937387,0.19483758456667441,0.42134660921126038,-0.62767919071448319,-0.12484437504934788,0.00054918965112282433,-0.42423017866187623,0.38978061544100862,-0.25889362580242559,0.14059206456390966,1.4396658692652198,0.09148521682694026,0.8304577254826675,-0.34167747308145668,0.27433085829910592,0.59433121902710195,0.16829388436581422,-0.39710315333910268,0.26436649476901036,-0.15815485676934332,-0.31568473204533481,-0.45955682660254932,0.36277556360773749},
             {0.52026929630787544,0.19976309526579114,0.048288534870342882,1.063431208236687,-0.33410059968511752,-0.38988221153520231,-0.52455251447243256,0.43016624216846461,0.61480764852690761,-1.2029556670797235,-0.30247805079126705,0.62618533043459412,-0.73158295881008972,-0.020572907378096739,0.19313583641996851,-0.60054522181956826,-0.12579188154529555,-0.93512857550623407,1.2648520462641502,-1.3627607284783472,0.21146837188286338,0.27512101099543229,-1.2813764947835664,0.31638430364145537,-0.020056231576856931,-0.77145892884793166,-0.62160973141903786,0.48017125545959766,-1.7202114377423279,0.20081266126274039,-0.99553578976828772,0.20845025359266153,0.77453377319162542,-0.021033789935355866,-0.23182399271535184,0.53022590464362174,0.86717311649694095,-0.45437317903751928,-0.26766492875799935,-0.2876848090459177,-0.51498121399701624,-1.2329961226183954,-0.10535577158961505},
             {0.10342216356484789,-0.14054342688478713,0.17349671069196868,0.22854504618497529,-0.18498252652113337,0.21051711132432316,-0.15263372488021612,-0.27015773464822712,0.042491323882085978,-0.11936395068103996,0.19685350733015042,0.26597491874908602,-0.38675994219399462,0.27890947553807494,0.070883606402987659,-0.25925710701307714,-0.15084968890108008,-0.26099457429556411,0.24738562739511943,-0.038048655680129467,-0.16338387469608542,0.0038811702988313752,-0.048553398665391823,-0.21693763593817081,-0.18599598097415157,0.077326383440317656,-0.45442829862631667,0.10158647121272658,-0.27316936757427462,0.11005336998751777,-0.22590466483202293,-0.11074931616378854,-0.24460210348230293,0.44112408940941461,-0.36185408250584539,-0.27182893645958367,-0.0039337092693952835,0.46256885773882717,0.29217038805090217,-0.23381776115087102},
             {0.11775404976088377,0.15984406700677739,0.44794472865763529,-0.22061851076042913,-0.013107698343781019,0.32199085851322423,-0.20641678715593847,-0.30390970805814888,-0.23211547179099445,0.37596667528268624,0.31794215715527785,0.078550096553006626,-0.15060716311403147,0.094047301181144308,-0.37274558170733318,-0.11167101630214549,-0.13012128613757873,-0.1814035418415921,0.24940627572972457,0.071048524928553763,-0.20853597342428937,0.38995419221568511,-0.017026994049369235,0.35673400808350458,-0.081016274860633924,0.15466323750884706,0.13892281427232259,-0.26513850182000648,0.13545691538795751,0.15537401325036415,0.22451214416225071,-0.48447591600603329,-0.53232906352117548,0.20829314311689182,-0.13854499730151384,-0.36831027592835225,-0.5645650583997871,0.078987423937690121,0.16415332852651437,0.12810029019512195}};

     //Layer2
     //2x1
     double[][] b2={{-0.22016129496901163},
             {-0.017231316717811088}};

     //2x5      from 2x20
     double[][] LW2_1={{-0.25410401023188761,-1.2448375823601698,-0.50622349813172418,-0.022879338409030725,0.59078854215623333},
             {-0.59853443388196947,0.96203851134944884,-0.30588419684315266,-0.4156570936303145,-0.33794655656149791}};

     //Dimensions
     //size(x1,2) is 1
     int Q= 1;

     // Input 1
     double[][] xp1 = new double[43][1];
     double[][] a1= new double[5][1];
     double[][] a2= new double[2][1];



     public DataListener(final WeakReference<Activity> activityRef) {
        this.activityRef = activityRef;

        //mellow and concentration
        x1[25][0]=0.0;
        x1[26][0]=0.0;

        savetoFileThread=new SavetoFileThread();
        savetoFileThread.start();

      wifiTask= new WifiSocketTask(arduinoIp,port);
         wifiTask.start();


        //bluetooth
         myBlueComms = new BlueComms();



     }

     /*
     * run the activity based on WIFI, Recording, Bluetooth
     * */
    @Override
    public void receiveMuseDataPacket(MuseDataPacket p) {
        switch (p.getPacketType()) {
            case ALPHA_ABSOLUTE:
                updateAlphaAbsolute(p.getValues());
                break;
            case ALPHA_RELATIVE:
                updateAlphaAbsolute(p.getValues());
                break;
            case BETA_RELATIVE:
                updateBetaRelative(p.getValues());
                break;
            case BETA_ABSOLUTE:
                updateBetaAbsolute(p.getValues());
                break;
            case GAMMA_RELATIVE:
                updateGammaRelative(p.getValues());
                break;
            case GAMMA_ABSOLUTE:
                updateGammaAbsolute(p.getValues());
                break;
            case DELTA_RELATIVE:
                updateDeltaRelative(p.getValues());
                break;
            case DELTA_ABSOLUTE:
                updateDeltaAbsolute(p.getValues());
                break;
            case THETA_RELATIVE:
                updateThetaRelative(p.getValues());
                break;
            case THETA_ABSOLUTE:
                updateThetaAbsolute(p.getValues());
                break;
            default:
                //various cases
               updateSavetoFileThread();
                // wifi
                museArduinoWifi();
                //bluetooth
                museArduinoBluetooth();
                break;
        }
    }

    @Override
    public void receiveMuseArtifactPacket(MuseArtifactPacket p) {
        if (p.getHeadbandOn() && p.getBlink()) {
          //  Log.i("Artifacts", "blinking");
            x1[16][0]=1.0;

           blinking ="1";
        } else {
            x1[16][0]=0.0;
            blinking ="0";
        }


      //write blink data for wifi activity
     /*if(wifiActivity) {
         counter++;
         if (counter > 2) {
             {
                 wifiTask.write(blinking);
                 counter = 0;
             }
         }

     }*/
        if (p.getHeadbandOn() && p.getJawClench()) {
           // Log.i("Artifacts", "Jaw Clench");
        }
    }


     private void updateAlphaAbsolute(final ArrayList<Double> data) {
         Activity activity = activityRef.get();
         if (activity != null) activity.runOnUiThread(new Runnable() {
             @Override
             public void run() {

                 //set the double Array to alpha relative values
                 x1[0][0]=data.get(Eeg.TP9.ordinal());
                 x1[1][0]=data.get(Eeg.FP1.ordinal());
                 x1[2][0]=data.get(Eeg.FP2.ordinal());
                 x1[3][0]=data.get(Eeg.TP10.ordinal());

             }
         });
     }
     private void updateAlphaRelative(final ArrayList<Double> data) {
         Activity activity = activityRef.get();
         if (activity != null) activity.runOnUiThread(new Runnable() {
             @Override
             public void run() {

                 //set the double Array to alpha relative values
                 x1[4][0]=data.get(Eeg.TP9.ordinal());
                 x1[5][0]=data.get(Eeg.FP1.ordinal());
                 x1[6][0]=data.get(Eeg.FP2.ordinal());
                 x1[7][0]=data.get(Eeg.TP10.ordinal());

             }
         });
     }
     private void updateBetaAbsolute(final ArrayList<Double> data) {
         Activity activity = activityRef.get();
         if (activity != null) activity.runOnUiThread(new Runnable() {
             @Override
             public void run() {

                 //set the double Array to alpha relative values
                 x1[8][0]=data.get(Eeg.TP9.ordinal());
                 x1[9][0]=data.get(Eeg.FP1.ordinal());
                 x1[10][0]=data.get(Eeg.FP2.ordinal());
                 x1[11][0]=data.get(Eeg.TP10.ordinal());

             }
         });
     }

    private void updateBetaRelative(final ArrayList<Double> data) {
        Activity activity = activityRef.get();
        if (activity != null) activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                //set the double Array to alpha relative values
                x1[12][0]=data.get(Eeg.TP9.ordinal());
                x1[13][0]=data.get(Eeg.FP1.ordinal());
                x1[14][0]=data.get(Eeg.FP2.ordinal());
                x1[15][0]=data.get(Eeg.TP10.ordinal());

            }
        });
    }





    private void updateDeltaAbsolute(final ArrayList<Double> data) {
        Activity activity = activityRef.get();
        if (activity != null) activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                //set the double Array to alpha relative values
                x1[17][0]=data.get(Eeg.TP9.ordinal());
                x1[18][0]=data.get(Eeg.FP1.ordinal());
                x1[19][0]=data.get(Eeg.FP2.ordinal());
                x1[20][0]=data.get(Eeg.TP10.ordinal());

            }
        });
    }


     private void updateDeltaRelative(final ArrayList<Double> data) {
         Activity activity = activityRef.get();
         if (activity != null) activity.runOnUiThread(new Runnable() {
             @Override
             public void run() {

                 //set the double Array to alpha relative values
                 x1[21][0]=data.get(Eeg.TP9.ordinal());
                 x1[22][0]=data.get(Eeg.FP1.ordinal());
                 x1[23][0]=data.get(Eeg.FP2.ordinal());
                 x1[24][0]=data.get(Eeg.TP10.ordinal());

             }
         });
     }

     private void updateGammaAbsolute(final ArrayList<Double> data) {
         Activity activity = activityRef.get();
         if (activity != null) activity.runOnUiThread(new Runnable() {
             @Override
             public void run() {

                 //set the double Array to alpha relative values
                 x1[27][0]=data.get(Eeg.TP9.ordinal());
                 x1[28][0]=data.get(Eeg.FP1.ordinal());
                 x1[29][0]=data.get(Eeg.FP2.ordinal());
                 x1[30][0]=data.get(Eeg.TP10.ordinal());

             }
         });
     }

     private void updateGammaRelative(final ArrayList<Double> data) {
        final Activity activity = activityRef.get();
        if (activity != null) activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                //show eeg values if dataRecording Activity
                if(dataRecordingActivity){
                 elem1 = (TextView) activity.findViewById(R.id.elem1);
                 elem2 = (TextView) activity.findViewById(R.id.elem2);
                 elem3 = (TextView) activity.findViewById(R.id.elem3);
                 elem4 = (TextView) activity.findViewById(R.id.elem4);


                elem1.setText(String.format(
                        "%6.2f",data.get( Eeg.TP9.ordinal())));
                elem2.setText(String.format(
                       "%6.2f", data.get(Eeg.FP1.ordinal())));
                elem3.setText(String.format(
                        "%6.2f", data.get(Eeg.FP2.ordinal())));
                elem4.setText(String.format(
                        "%6.2f", data.get(Eeg.TP10.ordinal())));
                        }
                x1[31][0]=data.get(Eeg.TP9.ordinal());
                x1[32][0]=data.get(Eeg.FP1.ordinal());
                x1[33][0]=data.get(Eeg.FP2.ordinal());
                x1[34][0]=data.get(Eeg.TP10.ordinal());

            }
        });
    }

     private void updateThetaAbsolute(final ArrayList<Double> data) {
         Activity activity = activityRef.get();
         if (activity != null) activity.runOnUiThread(new Runnable() {
             @Override
             public void run() {

                 //set the double Array to alpha relative values
                 x1[35][0]=data.get(Eeg.TP9.ordinal());
                 x1[36][0]=data.get(Eeg.FP1.ordinal());
                 x1[37][0]=data.get(Eeg.FP2.ordinal());
                 x1[38][0]=data.get(Eeg.TP10.ordinal());

             }
         });
     }

    private void updateThetaRelative(final ArrayList<Double> data) {
        final Activity activity = activityRef.get();
        if (activity != null) activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                //set the double Array to alpha relative values
                x1[39][0]=data.get(Eeg.TP9.ordinal());
                x1[40][0]=data.get(Eeg.FP1.ordinal());
                x1[41][0]=data.get(Eeg.FP2.ordinal());
                x1[42][0]=data.get(Eeg.TP10.ordinal());




            }
        });
    }

      private void museArduinoBluetooth(){
          final Activity activity= activityRef.get();

          if(activity!=null) activity.runOnUiThread(new Runnable(){

              @Override
              public void run(){

                  if(bluetoothActivity)
                  {
                      bluetoothbulbImageView = (ImageView) activity.findViewById(R.id.lefteyeImageView);
                     //calculate neural network value
                          //check the ideation
                          double[][] youtput= yOutput(getXin());


                          /*if(youtput[0][0]>0.5){

                              ideation="1";
                              bluetoothbulbImageView.setBackgroundResource(R.drawable.bulbon);

                          }
                          else
                          {
                              ideation="0";
                              bluetoothbulbImageView.setBackgroundResource(R.drawable.bulboff);

                          }
*/

                          Log.d("Datalistener","youtput0" + youtput[0][0] +"youtput1" +youtput[1][0]);


                      arduinoBluetoothStatusView = (TextView) activity.findViewById(R.id.arduinoBluetoothStatusView);

                      arduinoBluetoothStatusView.setText("Ideation Percentage : "+String.format(
                                  "%6.2f",youtput[0][0]) +"\n     Idea : "+ideation );


                        if(bluetoothconnected) {


                            //try {
                               // myBlueComms.openBT();
                           // } catch (IOException ex) {
                             //   showMessage("IOEXception occured", activity);
                            //}

                           if (bluetoothsendData) {

                              //  wifistatusTextView.setText("Connected to bulb");



                               try{
                                   counter++;
                                   if (counter > 5) {
                                       {
                                    myBlueComms.findBT();
                                       myBlueComms.openBT();


                                   //take every second one

                                          // myBlueComms.sendData(ideation);

                                           myBlueComms.outputdata=ideation;
                                           counter = 0;

                                       }
                                   }
                              } catch (IOException ex){
                                  showMessage("Connect Failed due to IOException",activity);
                               }


                          } else {


                                // wifistatusTextView.setText("Disconnected from bulb!");
                             //  myBlueComms.setArduinoConnectedWifi(true);
                               showMessage("Stopped sending data",activity);



                           }
                        }
                      else{

                            try{
                               myBlueComms.closeBT();
                            } catch (IOException ex){
                                showMessage("Close Failed due to IOException",activity);
                            }
                        }


                      }

                  }


          });

      }

     private void showMessage(String theMsg, Activity activity) {


         Toast msg = Toast.makeText(activity.getBaseContext(),
                 theMsg, Toast.LENGTH_LONG);
         msg.show();
     }


     private void museArduinoWifi() {
         final Activity activity = activityRef.get();


         if (activity != null) activity.runOnUiThread(new Runnable() {
             @Override
             public void run() {



                 if (wifiActivity) {

                     //start eye blinking
                     wifibulbImageView = (ImageView) activity.findViewById(R.id.lefteyeImageView);
                     //  righteyeImageView = (ImageView) activity.findViewById(R.id.righteyeImageView);

                     //calculate neural network value
                     //check the ideation
                     double[][] youtput= yOutput(getXin());


                     if(youtput[0][0]>0.5){

                         ideation="1";
                         wifibulbImageView.setBackgroundResource(R.drawable.bulbon);

                     }
                     else
                     {
                         ideation="0";
                         wifibulbImageView.setBackgroundResource(R.drawable.bulboff);

                     }

                     //take every second one
                     counter++;
                     if (counter > 2) {
                         {
                             wifiTask.write(ideation);
                             counter = 0;
                         }
                     }
                //     Log.d("Datalistener","youtput0" + youtput[0][0] +"youtput1" +youtput[1][0]);

                     //keep arduino wifi off
                     wifiTask.setArduinoConnectedWifi(true);

                     wifistatusTextView = (TextView) activity.findViewById(R.id.wifiStatusTextView);

                     wifistatusTextView.setText("Ideation Percentage : "+String.format(
                             "%6.2f",youtput[0][0]) +"\n     Idea : "+ideation );



                     if (wificonnected) {

                       //  wifistatusTextView.setText("Connected to bulb");

                            wifiTask.setArduinoConnectedWifi(false);


                     }
                         else {


                          //   wifistatusTextView.setText("Disconnected from bulb!");
                             wifiTask.setArduinoConnectedWifi(true);


                         }


                     } else
                     {
                      wifiTask.quit();
                     }
             }

         });
     }





// has been commented out
     private void updateSavetoFileThread(){

         final Activity activity = activityRef.get();
         if (activity != null) activity.runOnUiThread(new Runnable() {
             @Override
             public void run() {

                 if(dataRecordingActivity) {


                    if(startSession) {
                        EditText nameeditText = (EditText) activity.findViewById(R.id.nameeditText);
                        //Save file name for the entry
                        if (firstTimeLog) {
                            Calendar c = Calendar.getInstance();
                            int seconds = c.get(Calendar.SECOND);
                            int minute = c.get(Calendar.MINUTE);
                            int hour = c.get(Calendar.HOUR);

                            int date = c.get(Calendar.DATE);
                            int month = c.get(Calendar.MONTH);
                            logfilename = nameeditText.getText().toString() + "_" + month + date  + hour+ minute+ seconds;
                        }
                        firstTimeLog = false;

                        //add value of idea field if save button clicked

                        logfileText = getX1();

                        if (hasIdea) {

                                logfileText += "0, 1, 0";
                            }

                       else if (hasnoIdea) {

                                 logfileText += "1, 0, 0";
                            }

                         else {
                                logfileText += "0, 0, 1";
                               }

                        logfileText += "\n";

                        hasIdea = false;
                        hasnoIdea=false;
                        //write background thread
                        String[] text = {logfilename, logfileText};
                        savetoFileThread.write(text);
                    }
                     else{
                        firstTimeLog=true;
                    }
                 }
                 else
                 {
                     savetoFileThread.quit();
                 }

             }
         });
     }

     private double[][] yOutput(double[][] x1){

         //update x1 in a thread showcasing progressbar
         //hangs program code starts
         // ===== NEURAL NETWORK CONSTANTS initialization code=====

         //--- Input 1---
         // x1_step1_xoffset,x1_step1_gain,x1_step1_ymin


         // implement this, if (x1==nan){x1=0}

         // double[][] x1=new double[43][1];//m =43 n=0
         //for (int i = 0; i < 43; i++) {
         //  if(x1[i][0]==NAN){
         //    x1[i][0] = 1;}
         //}

         for (int i = 0; i < 40; i++) {
             x1_step1_xoffset[i][0] = x1_step_offset[i];
             x1_step1_gain[i][0] = x1_step_gain[i];
             x1_step1_ymin[i][0] = -1;
         }



         //layer1
         //layer2
         //already declared

         //======Simulation=======
         //dimension declared

         //Input 1
         xp1=mapminmax_apply(x1,x1_step1_gain,x1_step1_xoffset,x1_step1_ymin);

         //Layer1
         //not using repmat since b1 is the same as repmat(b1,1,1),
         // a1 = tansig_apply(repmat(b1,1,Q) + IW1_1*xp1)
         a1= tansig_apply(Matrix.add(b1, Matrix.multiply(IW1_1, xp1)));

         //Layer2
         //a2 = softmax_apply(repmat(b2,1,Q) + LW2_1*a1); not using repmat

         a2= softmax_apply(Matrix.add(b2, Matrix.multiply(LW2_1,a1)));

         //Output1
         // y1=a2;
         // y1= mean(y1,2); same as a2 in this case hence a2 is the answer




         Matrix.printMatrix(xp1);
         //hangs program code ends

         return a2;
     }

     //additonal functions for Matlab

     //additional functions to complete the matlab functionality




     //datalistener getter and setter
     public String getX1(){

         String textfile="";
         // make NULL and NAN as 0
         for (int i = 0; i < 43; i++) {
             if (String.valueOf(x1[i][0]) != null) {

             } else {
                 x1[i][0] = 0.0;
             }
             if (Double.isNaN(x1[i][0]))
                 x1[i][0] = 0.0;
             //      x1[i][0]=0.0;


             textfile += x1[i][0] + " , ";
             //  Log.d("check input", x1[i][0] + "input file is being stored");
         }


         return textfile;
     }




     public double[][] getXin(){

       int j=0;
         //skip 16,25,26
         for(int i=0;i<43;i++){


             //make null values zero
             if (String.valueOf(x1[i][0]) != null) {

             } else {
                 x1[i][0] = 0.0;
             }
             if (Double.isNaN(x1[i][0]))
                 x1[i][0] = 0.0;


            // Log.d("check input", x1[i][0] + "input file is being stored");
             //0-15
             /*if( i>=0 && i<16) {
                 xin[i][0] = x1[i][0];
             }
             //16-24
             else if(i>=17 && i<25){
                 xin[i-1][0] =x1[i][0];
             }
             //25-40
             else if(i>=27 && i<43){
                 xin[i-3][0] =x1[i][0];
             }*/
               if(i==16 ||i==25 ||i==26){
                   j++;
               }
             else {

                   xin[i - j][0] = x1[i][0];
               }

         }

         return xin;
     }

     private double[][] mapminmax_apply(double[][] x, double[][] settings_gain, double[][] settings_offset, double[][] settings_ymin) {
         double[][] y= new double[43][1];
         y=bsxfun("minus",x,settings_offset);
         y=bsxfun("times",y,settings_gain);
         y=bsxfun("plus",y,settings_ymin);
         return y;
     }

     private double[][] bsxfun(String symbol, double[][] first, double[][] settings) {

         if(symbol.equals("plus")){
             return( Matrix.add(first,settings));
         }
         else if(symbol.equals("minus")){
             return( Matrix.subtract(first, settings));
         }
         else if(symbol.equals("times")){
             return( Matrix.bMultiply(first, settings));
         }else if(symbol.equals("rdivide")){
             return (Matrix.rDivide(first, settings));
         }
         return first;
     }


     private double[][] tansig_apply(double[][] b) {
         int m=b.length;
         int n =b[0].length;
         double[][] a= new double[m][n];
         //  a = 2 ./ (1 + exp(-2*n)) - 1;

         //generate 2
         double[][] two= Matrix.identity(2,m,n);
         //generate 1
         double[][] one= Matrix.identity(1,m,n);

         //generate 1+exp(-2*n)
         double[][] den= Matrix.add(one, Matrix.expMatrix(Matrix.scalarMultiply(b,-2)));

         a= Matrix.subtract(Matrix.rDivide(two,den),one);


         return a;
     }

     private double[][] softmax_apply(double[][] b) {
         int m = b.length;
         int n=b[0].length;
         double[][]a=new double[m][n];

         //max is hardcoded
         //nmax = max(n,[],1);
         double b1=b[0][0];
         double b2=b[1][0];
         double nmax;
         if(b1>=b2)
         {
             nmax=b1;
         }else
             nmax=b2;

         //convert nmax to matrix
         double[][] nmaxMat= Matrix.identity(nmax,m,n);
         b=bsxfun("minus",b,nmaxMat);
         double[][] numer=Matrix.expMatrix(b);
         double denom= numer[0][0]+ numer[1][0];
         //denom(denom == 0) = 1;
         if(denom==0){denom=1;}
         double[][] denomMat= Matrix.identity(denom,m,n);
         a=bsxfun("rdivide",numer,denomMat);
         return a;
     }
     //helper function for tansig and softmax
     private int repmat(double[][] b1, int i, int q) {
         return i;
     }

     private double[][] mean(double[][] y1, int i) {
         return y1;
     }




     //bluetooth arduiono getters and setters
     public Boolean getBluetoothActivity() {
         return bluetoothActivity;
     }

     public void setBluetoothActivity(Boolean bluetoothActivity) {
         this.bluetoothActivity = bluetoothActivity;
     }

     public Boolean getBluetoothconnected() {
         return bluetoothconnected;
     }

     public void setBluetoothconnected(Boolean bluetoothconnected) {
         this.bluetoothconnected = bluetoothconnected;
     }

     public Boolean getBluetoothsendData() {
         return bluetoothsendData;
     }

     public void setBluetoothsendData(Boolean bluetoothsendData) {
         this.bluetoothsendData = bluetoothsendData;
     }


     //wifi arduino getters and setters
     public Boolean getWificonnected() {
         return wificonnected;
     }

     public void setWificonnected(Boolean wificonnected) {
         this.wificonnected = wificonnected;
     }
     public Boolean getWifiActivity() {
         return wifiActivity;
     }

     public void setWifiActivity(Boolean wifiActivity) {
         this.wifiActivity = wifiActivity;
     }

     public Boolean getWifiLightconnected() {
         return wifiLightconnected;
     }

     public void setWifiLightconnected(Boolean wifiLightconnected) {
         this.wifiLightconnected = wifiLightconnected;
     }

     // File writing getters and setters
     public Boolean getStartSession() {
         return startSession;
     }

     public void setStartSession(Boolean startSession) {
         this.startSession = startSession;
     }


     public Boolean getHasIdea() {
         return hasIdea;
     }

     public void setHasIdea(Boolean hasIdea) {
         this.hasIdea = hasIdea;
     }


     public Boolean getDataRecordingActivity() {
         return dataRecordingActivity;
     }

     public void setDataRecordingActivity(Boolean dataRecordingActivity) {
         this.dataRecordingActivity = dataRecordingActivity;
     }

     public Boolean getConnected() {
         return connected;
     }

     public void setConnected(Boolean connected) {
         this.connected = connected;
     }

     public Boolean getHasnoIdea() {
         return hasnoIdea;
     }

     public void setHasnoIdea(Boolean hasnoIdea) {
         this.hasnoIdea = hasnoIdea;
     }


}