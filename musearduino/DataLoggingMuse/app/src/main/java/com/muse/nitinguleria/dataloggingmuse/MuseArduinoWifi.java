package com.muse.nitinguleria.dataloggingmuse;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.ToggleButton;
import android.view.View.OnClickListener;
import com.interaxon.libmuse.ConnectionState;
import com.interaxon.libmuse.Muse;
import com.interaxon.libmuse.MuseDataPacketType;
import com.interaxon.libmuse.MuseManager;
import com.interaxon.libmuse.MusePreset;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import commusenitinguleriaMuseListeners.ConnectionListener;
import commusenitinguleriaMuseListeners.DataListener;

/**
 * Change Arduino IP in this class
 * */
public class MuseArduinoWifi extends Activity  implements OnClickListener {




    DataListener dataListener;
    ConnectionListener connectionListener;


    //Muse elements
    Button refreshButton;
    ToggleButton toggleConnectButton;
    Spinner museSpinner;

    //Muse library
    private Muse muse= null;
    //can be used to make pause /[play
    private boolean dataTransmission = true;

    //UI button wifi
    ToggleButton wifiConnect;



    public MuseArduinoWifi(){
        //listeners run on this activity
        WeakReference<Activity> weakActivity=new WeakReference<Activity>(this);
        connectionListener = new ConnectionListener(weakActivity);
        dataListener = new DataListener(weakActivity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_muse_arduino_wifi);

        wifiConnect=(ToggleButton) findViewById(R.id.wifiConnect);

        //Muse Buttons
        refreshButton=(Button) findViewById(R.id.refreshButton);
        toggleConnectButton=(ToggleButton) findViewById(R.id.museConnectButton);
        //MuseconnectButtonClicked function defined
        museSpinner = (Spinner) findViewById(R.id.museSpinner);

        // attach listener
        refreshButton.setOnClickListener(this);


       dataListener.setWifiActivity(true);
    }

    //implement toggle button clicks

    //connect button
    public void MuseconnectButtonClicked(View view){
        boolean on = ((ToggleButton) view).isChecked();
        if (on) {
            connectMuse();
        } else {
            if (muse != null) {
                muse.disconnect(true);
            }
        }
    }

    @Override
    public void onClick(View v){

        if(v.getId()==R.id.refreshButton){

            //change the background to X
            refreshButton.setBackgroundResource(R.drawable.cancelrefresh);
            //refresh
            MuseManager.refreshPairedMuses();

            //list of names
            List<Muse> pairedMuses = MuseManager.getPairedMuses();
            List<String> spinnerItems = new ArrayList<String>();

            //get names
            for (Muse m : pairedMuses) {
                String dev_id = m.getName() + "-" + m.getMacAddress();
                Log.i("Muse Headband", dev_id);
                spinnerItems.add(m.getName());

            }

            //insert names in spinner
            ArrayAdapter<String> adapterArray = new ArrayAdapter<String>(
                    this, android.R.layout.simple_spinner_item, spinnerItems);
            museSpinner.setAdapter(adapterArray);

            //change the background back to refresh icon
            refreshButton.setBackgroundResource(R.drawable.refresh);

        }


    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        dataListener.setWifiActivity(false);

    }
    @Override
    protected void onPause(){
        super.onPause();
        dataListener.setWifiActivity(false);

    }

    @Override
    protected void onResume(){
        super.onResume();
        dataListener.setWifiActivity(true);

    }




    // Connected State of Muse
    public void connectMuse(){
        List<Muse> pairedMuses = MuseManager.getPairedMuses();
        if (pairedMuses.size() < 1 ||
                museSpinner.getAdapter().getCount() < 1) {
            Log.w("Muse Headband", "There is nothing to connect to");
        } else {
            muse = pairedMuses.get(museSpinner.getSelectedItemPosition());
            ConnectionState state = muse.getConnectionState();
            if (state == ConnectionState.CONNECTED ||
                    state == ConnectionState.CONNECTING) {
                Log.w("Muse Headband", "doesn't make sense to connect second time to the same muse");
                return;
            }
            configure_library();
            /**
             * In most cases libmuse native library takes care about
             * exceptions and recovery mechanism, but native code still
             * may throw in some unexpected situations (like bad bluetooth
             * connection). Print all exceptions here.
             */
            try {
                muse.runAsynchronously();
            } catch (Exception e) {
                Log.e("Muse Headband", e.toString());

            }
        }
    }

    //helper  function for connectMuse()
    private void configure_library() {
        muse.registerConnectionListener(connectionListener);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.ACCELEROMETER);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.EEG);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.ALPHA_RELATIVE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.ALPHA_ABSOLUTE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.BETA_RELATIVE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.BETA_ABSOLUTE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.GAMMA_RELATIVE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.GAMMA_ABSOLUTE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.DELTA_RELATIVE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.DELTA_ABSOLUTE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.THETA_RELATIVE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.THETA_ABSOLUTE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.ARTIFACTS);

        muse.setPreset(MusePreset.PRESET_14);
        muse.enableDataTransmission(dataTransmission);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_muse_arduino_wifi, menu);
        return true;
    }

    public void wifiConnected(View view){

        Boolean connected=((ToggleButton) view).isChecked();
        if(connected){


            dataListener.setWificonnected(true);


        }else
        {
              dataListener.setWificonnected(false);

        }
    }

    /*public void wifiLightConnected(View view){

        Boolean connected=((ToggleButton) view).isChecked();
        if(connected){


            dataListener.setWifiLightconnected(true);


        }else
        {
            dataListener.setWifiLightconnected(false);

        }
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_record:
                intent = new Intent(this,DataRecording.class);
                startActivity(intent);
                return true;
            case R.id.action_bluetooth:
                intent= new Intent(this,MuseArduinoBluetooth.class);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
