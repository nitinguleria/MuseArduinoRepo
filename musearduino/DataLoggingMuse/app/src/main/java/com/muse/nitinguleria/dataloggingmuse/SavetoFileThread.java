package com.muse.nitinguleria.dataloggingmuse;

import android.os.*;
import android.os.Process;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by nitinguleria on 15-07-21.
 * Background Thread that reads and writes to the external storage file system in the default sdcard/ directory
 */

public class SavetoFileThread extends HandlerThread {

    private Handler backgroundAssistant; //handler is a worker or backgroundAssistant

    private static final int READ=1;
    private static final int WRITE=2;


    public SavetoFileThread() {

         super("SavetoFileThread", Process.THREAD_PRIORITY_BACKGROUND);
    }

    @Override
    protected void onLooperPrepared(){
        super.onLooperPrepared();
        backgroundAssistant = new Handler(getLooper()){

               @Override
                public void handleMessage(Message msg){
                   switch (msg.what){
                       case READ:
                          // backgroundAssistant.sendMessage()
                           break;
                       case WRITE:
                           String[] fileParameters= Arrays.copyOf((Object[]) msg.obj, 2, String[].class);
                           String filename=fileParameters[0];
                           String fileText=fileParameters[1];
                           appendLog(filename,fileText);
                           break;
                   }
               }
        };
    }

    /*
    * Initiate a read from the UI thread
    */
    public void read(){
        backgroundAssistant.sendEmptyMessage(READ);
    }
    /*
        * Write values from the UI thread
        */
    public void write(String[] writeText)
    {
        backgroundAssistant.sendMessage(Message.obtain(Message.obtain(backgroundAssistant, WRITE, writeText)));
    }


    public void appendLog(String name, String text) {
        File logFile = new File("sdcard/"+name+".txt");
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                Log.d("error", "Error: unable to create log file.");
                e.printStackTrace();
            }
        }

        try {
            // BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile,
                    true));
            buf.append(text);
            buf.newLine();
            buf.close();
            //Toast.makeText(this, "data written", Toast.LENGTH_LONG);
        } catch (IOException e) {
            Log.d("error", "Error: unable to write data to log file.");
            e.printStackTrace();
        }
    }


}
