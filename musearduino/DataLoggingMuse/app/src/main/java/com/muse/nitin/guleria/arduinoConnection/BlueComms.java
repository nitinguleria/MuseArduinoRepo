package com.muse.nitin.guleria.arduinoConnection;

import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.UUID;

/**
 * Created by Yaoming on 5/19/2015.
 */
public class BlueComms {  //extends Application
    BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;
    OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;
    byte[] readBuffer;
    int readBufferPosition;
    volatile boolean BTconnect = false;
    volatile boolean stopWorker;
    public String outputdata;

    TextView myLabel;
    Bitmap bitMap;
    ImageView image;
    int[] grid[] = new int [5][5];
    int[] ingrid[] = new int [5][5];
    Boolean InGame;
    int GameNum,size = 5;
    public void findBT() {
        if (BTconnect) return;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(mBluetoothAdapter == null) {
     //       myLabel.setText("Bluetooth Adapter not found");
        }
        if(!mBluetoothAdapter.isEnabled()) {
//            myLabel.setText("Bluetooth not open");
            return;
        }
      //  Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

        // print list of paired devices
        /*if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                String deviceBTName = device.getName();
                String deviceBTAddress = device.getAddress();
                String deviceBTMajorClass
                        = getBTMajorDeviceClass(device
                        .getBluetoothClass()
                        .getMajorDeviceClass());
              //btArrayAdapter.add(deviceBTName + "\n"
                     //   + deviceBTMajorClass);

              Log.d("Bluecomms"," Device class: " + deviceBTMajorClass+" Device Name : " +deviceBTName + "Device Address"+ deviceBTAddress);
            }
        }*/


        mmDevice = mBluetoothAdapter.getRemoteDevice("20:15:03:18:06:86"); // older 20:15:03:18:11:19
       /* if (pairedDevices.contains(mmDevice))
        {
            //System.out.println("WORKS FINE");
//            myLabel.setText("Bluetooth Device Found, address: " + mmDevice.getAddress());
            Log.d("ArduinoBT", "Bluetooth Device Found, address: " + mmDevice.getAddress());
        }*/
    }

    private String getBTMajorDeviceClass(int major){
        switch(major){
            case BluetoothClass.Device.Major.AUDIO_VIDEO:
                return "AUDIO_VIDEO";
            case BluetoothClass.Device.Major.COMPUTER:
                return "COMPUTER";
            case BluetoothClass.Device.Major.HEALTH:
                return "HEALTH";
            case BluetoothClass.Device.Major.IMAGING:
                return "IMAGING";
            case BluetoothClass.Device.Major.MISC:
                return "MISC";
            case BluetoothClass.Device.Major.NETWORKING:
                return "NETWORKING";
            case BluetoothClass.Device.Major.PERIPHERAL:
                return "PERIPHERAL";
            case BluetoothClass.Device.Major.PHONE:
                return "PHONE";
            case BluetoothClass.Device.Major.TOY:
                return "TOY";
            case BluetoothClass.Device.Major.UNCATEGORIZED:
                return "UNCATEGORIZED";
            case BluetoothClass.Device.Major.WEARABLE:
                return "AUDIO_VIDEO";
            default: return "unknown!";
        }
    }

    public void openBT() throws IOException {
        if (BTconnect)
            return;
        UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"); //Standard //SerialPortService ID, each Bluetooth Board has its own identity and UUID
        mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
        mmSocket.connect();
        mmOutputStream = mmSocket.getOutputStream();
        mmInputStream = mmSocket.getInputStream();
        BTconnect = true;
       transmitMuseData();
//        myLabel.setText("Bluetooth Opened");
        Log.d("Bluetooth","bluetoooth opened");
    }

    void beginListenForData(String outputdata) {
        final Handler handler = new Handler();
        final byte delimiter = 10; //This is the ASCII code for a newline character

        stopWorker = false;
        readBufferPosition = 0;
        readBuffer = new byte[1024];
        workerThread = new Thread(new Runnable() {
            public void run() {
                while(!Thread.currentThread().isInterrupted() && !stopWorker) {
                    try {
                        int bytesAvailable = mmInputStream.available();
                        if(bytesAvailable > 0) {
                            byte[] packetBytes = new byte[bytesAvailable];
                            mmInputStream.read(packetBytes);
                            for(int i=0;i<bytesAvailable;i++) {
                                byte b = packetBytes[i];
                                if(b == delimiter) {
                                    byte[] encodedBytes = new byte[readBufferPosition];
                                    System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                    final String data = new String(encodedBytes, "US-ASCII");
                                    readBufferPosition = 0;
                                    System.out.println(data);
                                    handler.post(new Runnable() {
                                        public void run() {
                                         //   myLabel.setText(data);
                                            //SizeChange();
                                           // ReceiveString();


                                            //if (InGame && ((GameNum == 1) || (GameNum == 3))) ReceiveString();
                                            //if (InGame && GameNum == 2) ReceiveStringG2();
                                        }
                                    });

                                }
                                else {
                                    readBuffer[readBufferPosition++] = b;
                                }
                            }
                        }
                    }
                    catch (IOException ex) {
                        stopWorker = true;
                    }
                }
            }
        });
        workerThread.start();



    }

    void transmitMuseData() {
        final Handler handler = new Handler();
        final byte delimiter = 10; //This is the ASCII code for a newline character

        stopWorker = false;
        readBufferPosition = 0;
        readBuffer = new byte[1024];
        workerThread = new Thread(new Runnable() {
            public void run() {
                while(!Thread.currentThread().isInterrupted() && !stopWorker) {
                       handler.post(new Runnable() {
                            public void run() {


                                try {
                                    mmOutputStream.write(outputdata.getBytes());
                                } catch (IOException e) {
                                    stopWorker = true;

                                    e.printStackTrace();
                                }

                                //if (InGame && ((GameNum == 1) || (GameNum == 3))) ReceiveString();
                                //if (InGame && GameNum == 2) ReceiveStringG2();
                            }
                        });



                }
            }
        });
        workerThread.start();



    }

    public void sendData(String data) throws IOException {
        if (BTconnect) {
          //  mmOutputStream.write(data.getBytes());
          //  myLabel.setText("Data Sent!:" + data);
        }
    }
    /*public void sendStrDataInLabel() throws IOException {
        if (BTconnect) {
            String msg = myTextbox.getText().toString();
            msg += "\n";
            mmOutputStream.write(msg.getBytes());
            //mmOutputStream.write('A');
            myLabel.setText("Data Sent");
        }
    }*/
    public void closeBT() throws IOException {
        if (BTconnect) {
            stopWorker = true;
            mmOutputStream.close();
            mmInputStream.close();
            mmSocket.close();
            BTconnect = false;
         //   myLabel.setText("Bluetooth Closed!");
        }
    }

}
