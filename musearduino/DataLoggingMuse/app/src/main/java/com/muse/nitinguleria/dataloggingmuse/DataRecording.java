package com.muse.nitinguleria.dataloggingmuse;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.view.View.OnClickListener;

import com.interaxon.libmuse.ConnectionState;
import com.interaxon.libmuse.Muse;
import com.interaxon.libmuse.MuseDataPacketType;
import com.interaxon.libmuse.MuseManager;
import com.interaxon.libmuse.MusePreset;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import commusenitinguleriaMuseListeners.ConnectionListener;
import commusenitinguleriaMuseListeners.DataListener;


/**
 * In this simple example MainActivity implements 2 MuseHeadband listeners
 * and updates UI when data from Muse is received. Similarly you can implement
 * listeners for other data or register same listener to listen for different type
 * of data.
 * For simplicity we create Listeners as inner classes of MainActivity. We pass
 * reference to MainActivity as we want listeners to update UI thread in this
 * example app.
 * You can also connect multiple muses to the same phone and register same
 * listener to listen for data from different muses. In this case you will
 * have to provide synchronization for data members you are using inside
 * your listener.
 *
 * Usage instructions:
 *
 * 1. Enable bluetooth on your device
 * 2. Pair your device with muse
 * 3. Run this project
 * 4. Press Refresh. It should display all paired Muses in Spinner
 * 5. Make sure Muse headband is waiting for connection and press connect.
 * It may take up to 10 sec in some cases.
 * 6. You should see EEG and accelerometer data as well as connection status,
 * Version information and MuseElements (alpha, beta, theta, delta, gamma waves)
 * on the screen.
 *
 *
 *
*/

/* some potential solutions to problems
* library linking error on build
* 1)Download existing muse project
* 2) Make sure you build the jni libs directory here and place it in the app folder.
 * 3)Download existing muse project Select android in the drop down from the app folder
 * 4)Create jniLibs directory similar to res directory
 *5) Move the armeabi and x86 folders to the jniLibs
*
* Now muse should run fine
* */

public class DataRecording extends Activity implements OnClickListener {


    // connect Muse
    private ConnectionListener connectionListener= null;

    //collect data from muse
    private DataListener dataListener=null;


    //Muse elements
    Button refreshButton;
    ToggleButton connectButton;
    Spinner museSpinner;


    //Logging session elements
     ToggleButton sessionButton;
      Button noIdeaButton,ideaButton;
    EditText nameeditText;
      TextView answerTextView;
      Boolean hasIdea = false;
     Boolean noIdea =false;
    Boolean firstTimeLog=true;
      double[][] museInputValues= new double[43][1];
      int ideaValue = -1; // 0 no idea,1 idea, -1 not recording


    //Muse library
    private Muse muse= null;
    //can be used to make pause /[play
    private boolean dataTransmission = true;

    //save to File
  //  SavetoFileThread savetoFileThread ;


    public DataRecording(){

     //listeners run on this activity
     WeakReference<Activity> weakActivity=new WeakReference<Activity>(this);
     connectionListener = new ConnectionListener(weakActivity);
     dataListener = new DataListener(weakActivity);
     }

    //Handler used by background thread for communication with main UI thread
    private Handler UIassistant=new Handler(){
        @Override
        public void handleMessage(Message msg){
            super.handleMessage(msg);
            if(msg.what==0){
                Integer i=(Integer) msg.obj;

            }
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_recording);

      //Muse Buttons
         refreshButton=(Button) findViewById(R.id.refreshButton);
         connectButton=(ToggleButton) findViewById(R.id.museConnectButton);
        //MuseconnectButtonClicked function defined
         museSpinner = (Spinner) findViewById(R.id.museSpinner);

      // attach listener
        refreshButton.setOnClickListener(this);





        //logging session elements
         sessionButton=(ToggleButton) findViewById(R.id.sessionButton);
        answerTextView=(TextView) findViewById(R.id.answerTextView);
        nameeditText=(EditText) findViewById(R.id.nameeditText);
        //sessionButtonClicked function defined
         ideaButton=(Button) findViewById(R.id.ideaButton);
        //ideaButtonClicked function defined
         noIdeaButton=(Button) findViewById(R.id.noIdeaButton);

        //initially  answer text, ideabutton and savebutton are invisible
        answerTextView.setVisibility(View.INVISIBLE);
        ideaButton.setVisibility(View.INVISIBLE);
        noIdeaButton.setVisibility(View.INVISIBLE);
    }


    //implement click Events
    @Override
    public void onClick(View v){

        if(v.getId()==R.id.refreshButton){

            //change the background to X
            refreshButton.setBackgroundResource(R.drawable.cancelrefresh);
            //refresh
            MuseManager.refreshPairedMuses();

            //list of names
            List<Muse> pairedMuses = MuseManager.getPairedMuses();
            List<String> spinnerItems = new ArrayList<String>();

            //get names
            for (Muse m : pairedMuses) {
                String dev_id = m.getName() + "-" + m.getMacAddress();
                Log.i("Muse Headband", dev_id);
                spinnerItems.add(m.getName());

            }

            //insert names in spinner
            ArrayAdapter<String> adapterArray = new ArrayAdapter<String>(
                    this, android.R.layout.simple_spinner_item, spinnerItems);
            museSpinner.setAdapter(adapterArray);

            //change the background back to refresh icon
            refreshButton.setBackgroundResource(R.drawable.refresh);

        }

       else if(v.getId()==R.id.ideaButton)
        {
            hasIdea = true;
            dataListener.setHasIdea(true);
            dataListener.setHasnoIdea(false);

            //set it to datalistener


            Toast.makeText(this,"response noIdea",Toast.LENGTH_SHORT).show();
       }
        else if(v.getId()==R.id.noIdeaButton)
        {
            noIdea =true;
            dataListener.setHasnoIdea(true);
            dataListener.setHasIdea(false);


        }




    }


    //implement toggle button clicks

    //connect button
    public void toggleConnectButtonClicked(View view){
        boolean on = ((ToggleButton) view).isChecked();
        if (on) {
            connectMuse();
        } else {
            if (muse != null) {
                muse.disconnect(true);
            }
        }
    }


    //session button
    public void sessionButtonClicked(View view){
        boolean on = ((ToggleButton) view).isChecked();
        if (on) {
               //make buttons visible
            answerTextView.setVisibility(View.VISIBLE);
            ideaButton.setVisibility(View.VISIBLE);
            noIdeaButton.setVisibility(View.VISIBLE);

            //set for datalistener
            dataListener.setStartSession(true);

        } else {

            //make buttons invisible
            answerTextView.setVisibility(View.INVISIBLE);
            ideaButton.setVisibility(View.INVISIBLE);
            noIdeaButton.setVisibility(View.INVISIBLE);


            //set for datalistener
            dataListener.setStartSession(false);

        }
    }

    //connect button
    public void MuseconnectButtonClicked(View view){
        boolean on = ((ToggleButton) view).isChecked();
        if (on) {
            connectMuse();
        } else {
            if (muse != null) {
                muse.disconnect(true);
            }
        }
    }


    // Connected State of Muse
    public void connectMuse(){
        List<Muse> pairedMuses = MuseManager.getPairedMuses();
        if (pairedMuses.size() < 1 ||
                museSpinner.getAdapter().getCount() < 1) {
            Log.w("Muse Headband", "There is nothing to connect to");
        } else {
            muse = pairedMuses.get(museSpinner.getSelectedItemPosition());
            ConnectionState state = muse.getConnectionState();
            if (state == ConnectionState.CONNECTED ||
                    state == ConnectionState.CONNECTING) {
                Log.w("Muse Headband", "doesn't make sense to connect second time to the same muse");
                return;
            }
            configure_library();
            /**
             * In most cases libmuse native library takes care about
             * exceptions and recovery mechanism, but native code still
             * may throw in some unexpected situations (like bad bluetooth
             * connection). Print all exceptions here.
             */
            try {
                muse.runAsynchronously();

            } catch (Exception e) {
                Log.e("Muse Headband", e.toString());

            }
        }
    }

    //helper  function for connectMuse()
    private void configure_library() {
        muse.registerConnectionListener(connectionListener);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.ACCELEROMETER);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.EEG);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.ALPHA_RELATIVE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.ALPHA_ABSOLUTE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.BETA_RELATIVE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.BETA_ABSOLUTE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.GAMMA_RELATIVE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.GAMMA_ABSOLUTE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.DELTA_RELATIVE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.DELTA_ABSOLUTE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.THETA_RELATIVE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.THETA_ABSOLUTE);
        muse.registerDataListener(dataListener,
                MuseDataPacketType.ARTIFACTS);

        muse.setPreset(MusePreset.PRESET_14);
        muse.enableDataTransmission(dataTransmission);
    }


  @Override
  protected void onDestroy(){
      super.onDestroy();

      dataListener.setDataRecordingActivity(false);
  }

    @Override
    protected void onResume(){
        super.onResume();
        dataListener.setDataRecordingActivity(true);

    }

    @Override
    protected void onPause(){
        super.onPause();

       dataListener.setDataRecordingActivity(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_data_recording, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {

            case R.id.action_bluetooth:
                 intent= new Intent(this,MuseArduinoBluetooth.class);
                startActivity(intent);
                return true;
            case R.id.action_wifi:
                 intent = new Intent(this,MuseArduinoWifi.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
